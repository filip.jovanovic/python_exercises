import mysql.connector
from mysql.connector import errorcode
from database import cursor


DB_NAME = 'store_management'

TABLES = {}

TABLES['inventory'] = (
    "CREATE TABLE `inventory` ("
    " `item_id` int AUTO_INCREMENT,"
    " `item_name` varchar(100) NOT NULL,"
    " `price` decimal(7,2),"
    " `quantity` decimal(7,2) NOT NULL,"
    " PRIMARY KEY (`item_id`)"
    ")"
)

TABLES['orders'] = (
    "CREATE TABLE `orders` ("
    " `order_id` int AUTO_INCREMENT,"
    " `item_id` int,"
    " `quantity_ordered` decimal(7,2) NOT NULL,"
    " `order_date` date DEFAULT (CURRENT_DATE),"
    " PRIMARY KEY (`order_id`),"
    " FOREIGN KEY (`item_id`) REFERENCES `inventory`(`item_id`)"
    ")"
)


def create_database():
    cursor.execute(
        "CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'"
        .format(DB_NAME))
    print("Database {} created!".format(DB_NAME))


def create_tables():
    cursor.execute("USE {}".format(DB_NAME))

    for table_name in TABLES:
        table_description = TABLES[table_name]
        try:
            print("Creating table ({}) ".format(table_name), end="")
            cursor.execute(table_description)
            print("Created table {}".format(table_name))
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Already Exists")
            else:
                print(err.msg)


# create_database()
# create_tables()

cursor.execute("DROP TABLE order")

