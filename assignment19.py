from database import cursor, db
from insert_data import get_inventory_except, get_orders_for_item


# update quantity in inventory
def update_inventory(item_id, new_quantity):
    sql = ("UPDATE inventory SET quantity = %s WHERE item_id = %s")
    cursor.execute(sql, (new_quantity, item_id))
    db.commit()
    print("Inventory updated")

# update quantity in orders
def update_order(order_id, new_quantity):
    sql = ("UPDATE orders SET quantity_ordered = %s WHERE order_id = %s")
    cursor.execute(sql, (new_quantity, order_id))
    db.commit()
    print("Orders updated")

# delete from inventory 
def delete_inventory_item(item_id):
    # Delete orders of item_id
    sql_orders = ("DELETE FROM orders WHERE item_id = %s")
    cursor.execute(sql_orders, (item_id, ))

    # Delete item
    sql_item = ("DELETE FROM inventory WHERE item_id = %s")
    cursor.execute(sql_item, (item_id, ))
    db.commit()
    print(f"Inventory item {item_id} deleted")

#delete order
def delete_order(order_id):
    sql = ("DELETE FROM orders WHERE order_id = %s")
    cursor.execute(sql, (order_id, ))
    db.commit()
    print(f"Order {order_id} deleted")

update_inventory(1, 5)
update_order(1, 1)
delete_inventory_item(1)
delete_order(2)