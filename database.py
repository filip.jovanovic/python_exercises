import mysql.connector


config = {
    'user': 'root',
    'password': 'password',
    'host': 'localhost',
    'database': 'store_management'
}

db = mysql.connector.connect(**config)
cursor = db.cursor()
