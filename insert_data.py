from database import cursor, db


def add_inventory(item_name, price, quantity):
    sql = ("INSERT INTO inventory(item_name, price, quantity) VALUES (%s, %s, %s)")
    cursor.execute(sql, (item_name, price, quantity,))
    db.commit()
    inventory_id = cursor.lastrowid
    print(f"Added inventory item {inventory_id}")

def add_orders(item_id, quantity, order_date=None):
    if order_date:
        sql = ("INSERT INTO orders(item_id, quantity_ordered, order_date) VALUES (%s, %s, %s)")
        cursor.execute(sql, (item_id, quantity, order_date))
    else:
        sql = ("INSERT INTO orders(item_id, quantity_ordered) VALUES (%s, %s)")
        cursor.execute(sql, (item_id, quantity))
    db.commit()
    order_id = cursor.lastrowid
    print(f"Added order {order_id}")

def get_inventory_except(item_id):
    sql = ("SELECT * FROM inventory WHERE item_id <> %s")
    cursor.execute(sql, (item_id, ))
    result = cursor.fetchall()
    return result

def get_orders_for_item(item_id):
    sql = ("SELECT * FROM orders WHERE item_id = %s")
    cursor.execute(sql, (item_id, ))
    result = cursor.fetchall()
    return result

def check_order_quantity(order_id):
    sql_ordered = ("SELECT quantity_ordered, item_id FROM orders WHERE order_id = %s")
    cursor.execute(sql_ordered, (order_id, ))
    quantity_ordered, item_id = cursor.fetchone()

    sql_available = ("SELECT quantity FROM inventory WHERE item_id = %s")
    cursor.execute(sql_available, (item_id, ))
    quantity_available = cursor.fetchone()[0]

    return quantity_available >= quantity_ordered


# cursor.execute("DELETE FROM orders WHERE item_id = 1")

# add_inventory('Laptop', 900, 20)
# add_inventory('Slusalice', 20, 30)
# add_inventory('Rokovnik', 9.5, 50)
# add_inventory('Hemijska olovka', 2.99, 100)
# add_inventory('Tastatura', 14, 40)

# add_orders(1, 4)
# add_orders(2, 9, '2023-12-31')
# add_orders(3, 10, '2020-01-01')
# add_orders(4, 40, '2021-05-05')
# add_orders(5, 3, '2024-01-01')

# invetnory_items = get_inventory_except(1)
# orders_for_item = get_orders_for_item(2)


# print(check_order_quantity(2))
